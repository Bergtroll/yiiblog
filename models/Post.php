<?php
namespace schmidtdenktmit\yiiblog\models;
use Yii;

/**
 * This is the model class for table "blog_post".
 *
 * The followings are the available columns in table 'blog_post':
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $create_time
 * @property string $update_time
 * @property string $subject
 * @property string $content
 */
class Post extends \CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    $blogModule = Yii::app()->getModule('blog');
    return '{{' . $blogModule->tablePrefix . $blogModule->postTable . '}}';
  }

  public function behaviors() {
    return array(
      'CTimestampBehavior' => array(
        'class' => 'zii.behaviors.CTimestampBehavior',
        'setUpdateOnCreate' => true,
        'timestampExpression' => 'date("Y-m-d H:i:s")',
      )
    );
  }

  public function rules() {
    return array(
      array('author_id, create_time, update_time', 'unsafe'),
      array('subject, content', 'required'),
      array('subject', 'length', 'max' => 80),
      $this->getSearchValidationRules(),
    );
  }

  private function getSearchValidationRules() {
    return array('id, author_id, create_time, update_time, subject, content', 'safe', 'on' => 'search');
  }

  public function relations() {
    $authorClass = Yii::app()->getModule('blog')->authorClass;
    return array(
      'author' => array(self::BELONGS_TO, $authorClass, 'author_id'),
      'comments' => array(self::HAS_MANY, 'schmidtdenktmit\yiiblog\models\Comment', 'blog_post_id'),
    );
  }

  public function attributeLabels() {
    return array(
      'id' => 'ID',
      'author_id' => 'Autor',
      'create_time' => 'Erstellt am',
      'update_time' => 'Bearbeitet am',
      'subject' => 'Newstitel',
      'content' => 'Beitrag',
    );
  }

  public function search() {
    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('author_id', $this->author_id);
    $criteria->compare('create_time', $this->create_time, true);
    $criteria->compare('update_time', $this->update_time, true);
    $criteria->compare('subject', $this->subject, true);
    $criteria->compare('content', $this->content, true);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }

  public function scopes() {
    return array(
      'aliased' => array(
        'alias' => 'sdm_blog_post',
      ),
      'chronologic' => array(
        'order' => 'sdm_blog_post.create_time DESC',
      ),
      'recent' => array(
        'limit' => Yii::app()->getModule('blog')->recentPostsLimit,
      ),
    );
  }

  public function recentSubjects() {
    $criteria = new CDbCriteria();
    $criteria->select = 'id, subject';
    $posts = $this->recently()->findAll();
    $subjects = array();
    foreach ($posts as $post) {
      $subjects[$post->id] = $post->subject;
    }
    return $subjects;
  }

}
