<?php
namespace schmidtdenktmit\yiiblog\models;
use CCaptcha;
use Yii;

/**
 * This is the model class for table "blog_comment".
 *
 * The followings are the available columns in table 'blog_comment':
 *
 * @property integer $id
 * @property integer $blog_post_id
 * @property integer $author_id
 * @property string $create_time
 * @property string $update_time
 * @property string $content
 * @property string $url
 * @property string $email
 * @property boolean $deleted
 */
class Comment extends \CActiveRecord {

  public $verifyCode;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    $blogModule = Yii::app()->getModule('blog');
    return '{{' . $blogModule->tablePrefix . $blogModule->commentTable . '}}';
  }

  public function behaviors() {
    return array(
      'CTimestampBehavior' => array(
        'class' => 'zii.behaviors.CTimestampBehavior',
        'setUpdateOnCreate' => TRUE,
        'timestampExpression' => 'date("Y-m-d H:i:s")',
      )
    );
  }

  public function rules() {
    return array(
      array('blog_post_id, author_id, create_time, update_time', 'unsafe'),
      //array('content', 'filter', 'filter' => array(Yii::app()->input, 'purify')),
      array('content', 'required'),
      array('content', 'length', 'max' => 2000),
      array('url', 'length', 'max' => 255),
      array('url', 'match', 'pattern' => '#^http\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(/\S*)?$#', 'on' => array('create')),
      array('email', 'length', 'max' => 128),
      array('email', 'email', 'on' => array('create')),
      array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'on' => array('create')),
      $this->getSearchValidationRules(),
    );
  }

  private function getSearchValidationRules() {
    return array('id, blog_post_id, author_id, create_time, update_time, content, url, email, deleted', 'safe', 'on' => 'search');
  }

  public function relations() {
    $authorClass = Yii::app()->getModule('blog')->authorClass;
    return array(
      'author' => array(self::BELONGS_TO, $authorClass, 'author_id'),
      'comments' => array(self::BELONGS_TO, 'schmidtdenktmit\yiiblog\models\Post', 'blog_post_id'),
    );
  }

  public function attributeLabels() {
    return array(
      'id' => 'ID',
      'blog_post_id' => 'BLOG_POST_ID',
      'author_id' => 'Autor',
      'create_time' => 'Erstellt am',
      'update_time' => 'Bearbeitet am',
      'content' => 'Mein Kommentar',
      'url' => 'URL',
      'email' => 'E-Mail',
      'verifyCode' => 'Verifizierungscode',
      'deleted' => 'Gelöscht',
    );
  }

  public function search() {
    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('blog_post_id', $this->blog_post_id);
    $criteria->compare('author_id', $this->author_id);
    $criteria->compare('create_time', $this->create_time, true);
    $criteria->compare('update_time', $this->update_time, true);
    $criteria->compare('content', $this->content, true);
    $criteria->compare('url', $this->url, true);
    $criteria->compare('email', $this->email, true);
    $criteria->compare('deleted', $this->deleted, true);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }

  public function scopes() {
    return array(
      'aliased' => array(
        'alias' => 'sdm_blog_comment',
      ),
      'chronologic' => array(
        'order' => 'sdm_blog_comment.create_time DESC',
      ),
    );
  }

}
