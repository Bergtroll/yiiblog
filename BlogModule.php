<?php
namespace schmidtdenktmit\yiiblog;

class BlogModule extends \CWebModule {

  public $name = 'Schmidt Denkt Mit - Yiiblog';
  public $description = 'A blog module for the Yii Web Programming Framework';
  public $version = '0.0.1';
  public $controllerNamespace = 'schmidtdenktmit\yiiblog\controllers';
  public $defaultController = 'Post';
  public $tablePrefix = 'sdm_blog_';
  public $postTable = 'post';
  public $commentTable = 'comment';
  public $authorClass = 'User';
  public $recentPostsLimit = 5;

/*    array(
    'post' => array(
      'name' => 'post',
    ),
    'comment' => array(
      'name' => 'comment',
    ),
    'author_class' => 'application.models.User',
  );*/

  public function init() {
    // this method is called when the module is being created
    // you may place code here to customize the module or the application
    // import the module-level models and components
    /*$this->setImport(array(
      'blog.models.*',
      'blog.components.*',
    ));*/
  }

  public function beforeControllerAction($controller, $action) {
    if (parent::beforeControllerAction($controller, $action)) {
      // this method is called before any module controller action is performed
      // you may place customized code here
      return true;
    } else
      return false;
  }

}
