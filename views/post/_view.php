<?php
$postHtml = <<<EOF
\n<h2 class="blog-post-subject">%s</h2>%s
<div class="blog-post-timestamp">Erstellt: %s</div>
<div class="blog-post-content">%s</div>
<div class="blog-post-comments">%s</div>
EOF;
if (true || Yii::app()->user->isAdmin)
{
    $edit = CHtml::link('Bearbeiten', array('/blog/post/update', 'id' => $data->id)) .' | ' . CHtml::link('Löschen', array('/blog/post/delete', 'id' => $data->id));
} else {
    $edit = '';
}

$formatter = new CDateFormatter('de_DE');
$timestamp = $formatter->formatDateTime(strtotime($data->create_time));
$comments[] = CHtml::link('Kommentieren', array('/blog/comment/create', 'id' => $data->id));
foreach ($data->comments(array('order' => 'create_time DESC')) as $comment) {
    $content = CHtml::openTag('span', array('class' => 'blog-post-comment-head'));
    $content .= ($comment->deleted)? 'Gelöscht von ' : 'Antwort von ';
    $content .= $comment->author->username;
    $content .= ' am ' . $formatter->formatDateTime(strtotime($comment->update_time));;
    /*if (Yii::app()->user->isAdmin) {
        $content .= CHtml::link(' Bearbeiten', array('blogComment/delete', 'id' => $comment->id));
    } */
    $content .= CHtml::closeTag('span');
    $content .= CHtml::tag('br') . $comment->content;
    $comments[] = CHtml::tag('li', array(), $content);
}
$comments = CHtml::tag('ol', array(), implode("\n", $comments));
printf($postHtml, $data->subject, $edit, $timestamp, $data->content, $comments);