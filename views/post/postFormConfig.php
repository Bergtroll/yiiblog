<?php

return array(
    'title' => 'Neuen Blogpost anlegen',
    'elements' => array(
        'subject' => array(
            'type' => 'text',
            'size' => 8,
            'maxlength' => 80,
        ),
        'content' => array(
            'type' => 'textarea',
        ),
    ),
    'buttons' => array(
        'create' => array(
            'type' => 'submit',
            'label' => 'Speichern',
            'class' => 'ym-success',
        ),
        'update' => array(
            'type' => 'submit',
            'label' => 'Ändern',
        ),
    ),
);
