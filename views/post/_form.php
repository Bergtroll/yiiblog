<?php
$buttons = $form->getButtons();
$model = $form->getModel();
$renderer = new FormInputRenderer($form);
?>
<?php echo $form->renderBegin(); ?>
<fieldset>
    <legend><?php echo $form->title; ?></legend>
    <?php echo $renderer->render('subject'); ?>
    <?php echo $renderer->render('content'); ?>
    <div class="ym-fbox-footer ym-fbox-button"">
        <?php
        echo ($model->isNewRecord) ? $buttons['create'] : $buttons['update'];
        ?>
    </div>
</fieldset>
<?php echo $form->renderEnd(); ?>
