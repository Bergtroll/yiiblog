<?php
$title = Yii::app()->name . ' - Blog';
$this->breadcrumbs = array(
    $this->module->id,
);
?>
<div class="blog-viewport">
    <h2><?php echo $title; ?></h2>
    <div class="blog-posts-list">
    <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$dataProvider,
            'itemView'=>'_view',
    )); ?>
    </div>    
</div>
