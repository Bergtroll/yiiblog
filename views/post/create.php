<?php
$this->breadcrumbs = array(
    $this->module->id,
);?>
<h1>Blogeintrag erstellen/bearbeiten</h1>
<?php echo $this->renderPartial('_form', array('form' => $form)); ?>