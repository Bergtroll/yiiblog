<?php
$buttons = $form->getButtons();
$originalComment = $form->model['content'];
$form->model['content'] = '';

$this->breadcrumbs = array(
    $this->module->id,
);
?>
<h1>Kommentar löschen</h1>

<?php echo $form->renderBegin(); ?>
<fieldset>
    <legend><?php echo $form->title; ?></legend>
    <label for="original-comment">Ursprünglicher Kommentar</label>
    <p id="original-comment"><?php echo $originalComment; ?></p>
    <div class="text-area">
        <label for="BlogComment_content" class="required">
            Löschkommentar <span class="required">*</span>
        </label>
        <?php echo $form['content']->renderInput(); ?>
        <?php echo $form['content']->renderError(); ?>
    </div>
    <div class="type-button" style="text-align: right;">
        <?php echo $buttons['delete']; ?>
    </div>
</fieldset>
<?php echo $form->renderEnd(); ?>