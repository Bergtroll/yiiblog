<?php
$buttons = $form->getButtons();
$renderer = new FormInputRenderer($form);
?>
<?php echo $form->renderBegin(); ?>
<fieldset>
    <legend><?php echo $form->title; ?></legend>
    <?php echo $renderer->render('content'); ?>
    <?php if(CCaptcha::checkRequirements()): ?>
	<div class="type-text">
		<?php echo $form['verifyCode']->renderLabel(); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form['verifyCode']->renderInput(); ?>
		</div>
		<div class="hint">
                    Um Missbrauch zu vermeiden, ist dieses Formular mit einem 
                    Captcha geschützt. Bitte gib die im obigen Bild gezeigten 
                    Buchstaben ein um deine Anfrage zu verifizieren. Auf Groß- 
                    und Kleinschreibung muss hierbei nicht geachtet werden.
                </div>
	</div>
	<?php endif; ?>
    <?php //echo $renderer->render('verifyCode'); ?>
    <div class="type-button" style="text-align: right;">
        <?php echo $buttons['create']; ?>
    </div>
</fieldset>
<?php echo $form->renderEnd(); ?>