<?php
return array(
    'title' => 'MoneyMedian Blogkommentar',
    'elements' => array(
        'content' => array(
            'type' => 'ext.elrte.ExtElRte',
            'selector' => 'BlogComment_content',
            'doctype' => '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">',
            'cssClass' => 'el-rte',
            'absoluteURLs' => 'false',
            'allowSource' => 'false',
            'lang' => 'de',
            'height' => '300',
            'styleWithCSS' => 'true',
            'fmAllow' => 'false',
            'toolbar' => 'commentToolbar',
            'connectorUrl' => '/test/elFinder',
        ),
        'url' => array(
            'type' => 'text',
            'size' => 50,
            'maxlength' => 255,
        ),
        'email' => array(
            'type' => 'text',
            'size' => 50,
            'maxlength' => 128,
        ),
        'type' => 'CCaptcha',
        'verifyCode' => array(
            'type' => 'text',
        )
    ),
    'buttons' => array(
        'create' => array(
            'type' => 'submit',
            'label' => 'Speichern',
        ),
        'delete' => array(
            'type' => 'submit',
            'label' => 'Löschen',
        ),
    ),
);