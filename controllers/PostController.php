<?php
namespace schmidtdenktmit\yiiblog\controllers;
use CActiveDataProvider;
use CDbCriteria;
use CForm;
use schmidtdenktmit\yiiblog\models\Comment;
use schmidtdenktmit\yiiblog\models\Post;
use Yii;


/**
 * Description of postController
 *
 * @author marius
 */
class PostController extends \Controller {

  public $layout = '//layouts/column2';

  public function filters() {
    return array(
      'accessControl',
    );
  }

  public function accessRules() {
    return array(
      array('allow',
        'actions' => array('index', 'view'),
        'users' => array('*'),
      ),
      array('allow',
        'actions' => array('create', 'update', 'delete'),
        'users' => array('*'),
      ),
      array('deny',
        'users' => array('*'),
      ),
    );
  }

  public function actionIndex() {
    $dataProvider = new CActiveDataProvider(Post::model()->aliased()->chronologic()->with('comments:aliased:chronologic'));
    $this->render('index', array('dataProvider' => $dataProvider));
  }

  public function actionView($id) {
    $criteria = new CDbCriteria();
    $criteria->alias = 'post';
    $criteria->with = 'comments:aliased:chronologic';
    $criteria->addCondition(array('id' => 'post.id=:id'));
    $criteria->params = array(':id' => $id);
    $dataProvider = new CActiveDataProvider('schmidtdenktmit\yiiblog\models\Post', array('criteria' => $criteria));
    $this->render('index', array('dataProvider' => $dataProvider, 'newComment' => new Comment));
  }

  public function actionCreate() {
    $post = new Post();
    $post->author_id = Yii::app()->user->id;
    $post->author_id = 10;
    $this->handleUserInput($post, 'create');
  }

  private function prepareForm(Post $post) {
    $form = new CForm('blog.views.post.postFormConfig', $post);
    $form->activeForm = array(
      'class' => 'CActiveForm',
      'id' => 'blog-post-form',
      'htmlOptions' => array('class' => 'ym-form post'),
      'enableAjaxValidation' => false,
      'errorMessageCssClass' => 'message',
    );
    return $form;
  }

  /**
   * @param $post
   * @param $action
   */
  private function handleUserInput($post, $action) {
    $form = $this->prepareForm($post);
    if ($form->submitted($action)) {
      $post = $form->getModel();
      if ($post->validate()) {
        $post->save(false);
        $this->redirect(array('/blog/post/view', 'id' => $post->id));
      }
    }
    $this->render('create', array('form' => $form));
  }

  public function actionUpdate($id) {
    $post = Post::model()->findByPk($id);
    $this->handleUserInput($post, 'update');
  }

  public function actionDelete($id) {
    $post = Post::model()->findByPk($id);
    $post->delete();
    $this->redirect(array('/blog/post/index'));
  }

}
