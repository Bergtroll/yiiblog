<?php
namespace schmidtdenktmit\yiiblog\controllers;
use \Yii;
use \CForm;
use \schmidtdenktmit\yiiblog\models\Comment;

/**
 * Description of BlogCommentController
 *
 * @author marius
 */
class CommentController extends \Controller {

    public $layout = '//layouts/column2';

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('create', 'captcha'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('delete'),
                'users' => array('mmadmin'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate($id) {
        $comment = new Comment();
        $form = $this->prepareForm($comment);
        if ($form->submitted('create')) {
            $comment = $form->getModel();
            $comment->author_id = 10;//Yii::app()->user->id;
            $comment->blog_post_id = $id;
            if ($comment->validate()) {
                $comment->save(false);
                $this->redirect(array('/blog/post/view', 'id' => $comment->blog_post_id));
            }
        }
        $this->render('create', array('form' => $form));
    }

    public function actionDelete($id) {
        $comment = Comment::model()->findByPk($id);
        $comment->scenario = 'delete';
        $form = $this->prepareForm($comment);
        $oldContent = $comment->content;
        if ($form->submitted('delete')) {
            $comment = $form->getModel();
            $comment->deleted = TRUE;
            $comment->url = 'Gelöscht';
            $comment->email = 'Gelöscht';
            $comment->author_id = Yii::app()->user->id;
            if ($comment->validate()) {
                $comment->update(array('content', 'deleted', 'url', 'email', 'author_id'));
                $this->redirect(array('/blog/post/view', 'id' => $comment->blog_post_id));
            }
        }
        $comment->content = $oldContent;
        $this->render('delete', array('form' => $form));
    }

    private function prepareForm(Comment $comment) {
        $form = new CForm('blog.views.comment.commentFormConfig', $comment);
        $form->activeForm = array(
            'class' => 'CActiveForm',
            'id' => 'blog-comment-form',
            'htmlOptions' => array('class' => 'yform blogcomment'),
            'enableAjaxValidation' => false,
            'errorMessageCssClass' => 'message',
        );
        return $form;
    }

//    public function actionDelete($id) {
//        $this->render('delete');
//    }
}
