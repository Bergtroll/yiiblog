<?php
namespace schmidtdenktmit\yiiblog\components\widgets;

/**
 * Description of NewsOutline
 *
 * @author marius
 */
class NewsOutline extends \CWidget {
    
    private $view = 'blog.components.widgets.views.newsOutline';
    
    public function run() {
        $subjects = BlogPost::model()->recentSubjects();
        $this->render($this->view,array('subjects' => $subjects));
    }
    
}
