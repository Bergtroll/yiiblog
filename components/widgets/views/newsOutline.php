<?php
$entry = '<div style="padding:10px 0px 10px 0px; border-bottom: 1px dotted #555555;">%s</div>';
foreach ($subjects as $id=>$subject) {
    printf($entry, CHtml::link($subject, array('/blog/blogPost/view', 'id'=>$id)));
}